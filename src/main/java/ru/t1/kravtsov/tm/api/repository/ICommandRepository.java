package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
