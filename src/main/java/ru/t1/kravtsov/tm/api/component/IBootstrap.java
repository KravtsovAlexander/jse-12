package ru.t1.kravtsov.tm.api.component;

public interface IBootstrap {

    void run(String[] args);

}
