package ru.t1.kravtsov.tm.component;

import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.api.controller.ICommandController;
import ru.t1.kravtsov.tm.api.controller.IProjectController;
import ru.t1.kravtsov.tm.api.controller.IProjectTaskController;
import ru.t1.kravtsov.tm.api.controller.ITaskController;
import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.ICommandService;
import ru.t1.kravtsov.tm.api.service.IProjectService;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.api.service.ITaskService;
import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;
import ru.t1.kravtsov.tm.controller.CommandController;
import ru.t1.kravtsov.tm.controller.ProjectController;
import ru.t1.kravtsov.tm.controller.ProjectTaskController;
import ru.t1.kravtsov.tm.controller.TaskController;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kravtsov.tm.exception.system.CommandNotSupportedException;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.repository.CommandRepository;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.service.CommandService;
import ru.t1.kravtsov.tm.service.ProjectService;
import ru.t1.kravtsov.tm.service.ProjectTaskService;
import ru.t1.kravtsov.tm.service.TaskService;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    @Override
    public void run(String[] args) {
        initDemoData();
        if (parseArguments(args)) exit();
        parseCommands();
    }

    private void initDemoData() {
        projectService.add(new Project("BETA PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("ALPHA PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("GAMMA PROJECT", Status.COMPLETED));

        taskService.add(new Task("DELTA TASK"));
        taskService.add(new Task("EPSILON TASK"));
    }

    private boolean parseArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        parseArgument(arg);
        return true;
    }

    private void parseCommands() {
        commandController.displayWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;

        switch (arg) {
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;

        switch (command) {
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.displayProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_DISPLAY_BY_ID:
                projectController.displayProjectById();
                break;
            case TerminalConst.PROJECT_DISPLAY_BY_INDEX:
                projectController.displayProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectsByName();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.displayTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_DISPLAY_BY_ID:
                taskController.displayTaskById();
                break;
            case TerminalConst.TASK_DISPLAY_BY_INDEX:
                taskController.displayTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTasksByName();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID:
                taskController.displayTasksByProjectId();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

}
